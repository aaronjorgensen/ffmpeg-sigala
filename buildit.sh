#!/bin/sh

set -e
set -u

jflag=
jval=2

while getopts 'j:' OPTION
do
  case $OPTION in
  j)	jflag=1
        	jval="$OPTARG"
	        ;;
  ?)	printf "Usage: %s: [-j concurrency_level] (hint: your cores + 20%%)\n" $(basename $0) >&2
		exit 2
		;;
  esac
done
shift $(($OPTIND - 1))

if [ "$jflag" ]
then
  if [ "$jval" ]
  then
    printf "Option -j specified (%d)\n" $jval
  fi
fi

cd `dirname $0`

BUILD_DIR="./build"
TARGET_DIR="./target"



# NOTE: this is a fetchurl parameter, nothing to do with the current script
#export TARGET_DIR_DIR="$BUILD_DIR"

echo "#### FFmpeg static build for CloudNcode ####"
cd $BUILD_DIR
../fetchurl "http://www.tortall.net/projects/yasm/releases/yasm-1.2.0.tar.gz"
../fetchurl "http://zlib.net/zlib-1.2.8.tar.gz"
../fetchurl "http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz"
../fetchurl "http://downloads.sf.net/project/libpng/libpng15/older-releases/1.5.14/libpng-1.5.14.tar.gz"
../fetchurl "http://downloads.xiph.org/releases/ogg/libogg-1.3.1.tar.gz"
../fetchurl "http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.3.tar.gz"
../fetchurl "http://downloads.xiph.org/releases/theora/libtheora-1.1.1.tar.bz2"
../fetchurl "http://webm.googlecode.com/files/libvpx-v1.1.0.tar.bz2"
../fetchurl "http://downloads.sourceforge.net/project/faac/faac-src/faac-1.28/faac-1.28.tar.bz2"
wget "http://ffmpeg.zeranoe.com/builds/source/ffmpeg/ffmpeg-20141021-git-e397edb.tar.xz"
../fetchurl "http://downloads.xvid.org/downloads/xvidcore-1.3.2.tar.gz"
../fetchurl "http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz"
../fetchurl "http://downloads.xiph.org/releases/opus/opus-1.1.tar.gz"
../fetchurl "http://openjpeg.googlecode.com/svn/trunk"
../fetchurl "http://downloads.xiph.org/releases/celt/celt-0.11.1.tar.gz"
../fetchurl "http://cloudncode.com/sources/external_libraries/bzip2-1.0.6.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/fontconfig-2.11.1.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/frei0r-20130909-git-10d8360.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/gnutls-3.2.15.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libiconv-1.14.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/freetype-2.5.3.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/gme-0.6.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libbluray-0.6.1.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libbs2b-3.1.0.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libcaca-0.99.beta18.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libgsm-1.0.13-4.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libilbc-20120913-git-b5f9b10.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libmodplug-0.8.8.5.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libwebp-0.4.0.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/libass-0.11.2.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/opencore-amr-0.1.3.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/openjpeg-1.5.1.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/opus-1.1.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/rtmpdump-20140707-git-a1900c3.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/schroedinger-1.0.11.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/soxr-0.1.1.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/speex-1.2rc1.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/twolame-0.3.13.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/vid.stab-0.98.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/vo-aanenc-0.1.3.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/vo-amrwbenc-0.1.2.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/wavepack-4.70.0.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/x265-1.3.tar.xz"
../fetchurl "http://cloudncode.com/sources/external_libraries/xavs-svn-r55.tar.xz"


hg clone https://bitbucket.org/multicoreware/x265
git clone https://github.com/ob-encoder/fdk-aac.git

echo "downloading Tmod"
cp ../x264_tMod-clone.sh $BUILD_DIR
sudo sh ./x264_tMod-clone.sh

echo "*** Building yasm ***"
cd $BUILD_DIR/yasm*
./configure --prefix=$TARGET_DIR
make -j $jval
make install

echo "*** Building yasm ***"
cd $BUILD_DIR/yasm*
./configure --prefix=$TARGET_DIR
make -j $jval
make install

echo "*** Building Celt ***"
cd $BUILD_DIR/celt*
./configure --prefix=$TARGET_DIR
make -j $jval
make install

echo "*** Building x265 ***"
cd $BUILD_DIR/x265*/build/linux
./make-Makefiles.bash --prefix=$TARGET_DIR
make -j $jval
make install



echo "*** Building fdk-aac ***"
cd $BUILD_DIR/fdk-aac*
autoreconf -i && ./configure --prefix=/usr --enable-shared
make -j5 && sudo make install



echo "*** Building zlib ***"
cd $BUILD_DIR/zlib*
./configure --prefix=$TARGET_DIR
make -j $jval
make install

echo "*** Building bzip2 ***"
cd $BUILD_DIR/bzip2*
make
make install PREFIX=$TARGET_DIR

echo "*** Building libpng ***"
cd $BUILD_DIR/libpng*
./configure --prefix=$TARGET_DIR --enable-static --disable-shared
make -j $jval
make install

# Ogg before vorbis
echo "*** Building libogg ***"
cd $BUILD_DIR/libogg*
./configure --prefix=$TARGET_DIR --enable-static --disable-shared
make -j $jval
make install

# Vorbis before theora
echo "*** Building libvorbis ***"
cd $BUILD_DIR/libvorbis*
./configure --prefix=$TARGET_DIR --enable-static --disable-shared
make -j $jval
make install

echo "*** Building libtheora ***"
cd $BUILD_DIR/libtheora*
./configure --prefix=$TARGET_DIR --enable-static --disable-shared
make -j $jval
make install

echo "*** Building livpx ***"
cd $BUILD_DIR/libvpx*
./configure --prefix=$TARGET_DIR --disable-shared
make -j $jval
make install

echo "*** Building faac ***"
cd $BUILD_DIR/faac*
./configure --prefix=$TARGET_DIR --enable-static --disable-shared
# FIXME: gcc incompatibility, does not work with log()

sed -i -e "s|^char \*strcasestr.*|//\0|" common/mp4v2/mpeg4ip.h
make -j $jval
make install

echo "*** Building x264 ***"
cd $BUILD_DIR/x264*
./configure --prefix=$TARGET_DIR --enable-static --disable-shared --enable-opencl --disable-avi-output
make -j $jval
make install

echo "*** Building xvidcore ***"
cd "$BUILD_DIR/xvidcore/build/generic"
./configure --prefix=$TARGET_DIR --enable-static --disable-shared
make -j $jval
make install
#rm $TARGET_DIR/lib/libxvidcore.so.*

echo "*** Building lame ***"
cd $BUILD_DIR/lame*
./configure --prefix=$TARGET_DIR --enable-static --disable-shared
make -j $jval
make install

echo "*** Building opus ***"
cd $BUILD_DIR/opus*
./configure --prefix=$TARGET_DIR --enable-static --disable-shared
make -j $jval
make install

# FIXME: only OS-specific
rm -f "$TARGET_DIR/lib/*.dylib"
rm -f "$TARGET_DIR/lib/*.so"

# FFMpeg
 echo "*** Building FFmpeg ***"
 cd $BUILD_DIR/ffmpeg*
CFLAGS="-I$TARGET_DIR/include" LDFLAGS="-L$TARGET_DIR/lib -lm" ./configure --prefix=${OUTPUT_DIR:-$TARGET_DIR} --extra-cflags="-I## $TARGET_DIR/include -static" --extra-ldflags="-L$TARGET_DIR/lib -lm -static" --extra-version=static --disable-debug --disable-shared --enable-static --extra-cflags=--static --enable-ffplay --disable-ffserver --enable-ffprobe --enable-libsmbclient --disable-doc --enable-gpl --enable-pthreads --enable-postproc --enable-gray --enable-runtime-cpudetect --enable-libfaac --enable-libmp3lame --enable-libopus --enable-libtheora --enable-libvorbis  --enable-libxvid --enable-bzlib --enable-zlib --enable-nonfree --enable-version3 --enable-libvpx  --enable-libfdk_aac --enable-libopenjpeg --enable-libvo-aacenc --enable-libtwolame --enable-libwavpack --enable-libx265 --enable-libzvbi --enable-avxsynth --enable-libvidstab
make -j $jval && make install
